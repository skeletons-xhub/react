# Install dependencies
FROM node:8-alpine AS builder

RUN npm install -g yarn@1.6.0

WORKDIR /usr/x-app
COPY package.json ./
COPY yarn.lock ./
RUN yarn install
COPY . ./
RUN yarn run build

# Copy build result
FROM nginx:1.17.0-alpine
EXPOSE 80
COPY --from=builder /usr/x-app/.nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /usr/x-app/build /usr/share/nginx/html
